package test.tshabalala.bongani.readjson;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import test.tshabalala.bongani.readjson.adapter.DeviceAdapter;
import test.tshabalala.bongani.readjson.helper.JSONParser;
import test.tshabalala.bongani.readjson.helper.Versions;

public class MainActivity extends AppCompatActivity {

    private ProgressDialog pDialog;
    private ListView listView;
    private List<Versions> listVersion = null;
    private static String url = "http://codetest.cobi.co.za/androids.json";
    private String path = "http://codetest.cobi.co.za/";
    JSONParser jsonParser = new JSONParser();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        listView = (ListView)findViewById(R.id.listView);
        DeviceAdapter.OnItemClickListener versionClickListener = new DeviceAdapter.OnItemClickListener() {
            @Override
            public boolean onItemClick(Versions versions) {
                Intent intent = new Intent(MainActivity.this,DisplayActivity.class);
                intent.putExtra("version",versions);

                startActivity(intent);
                return true;
            }

        };

            new retrieveData().execute();



    }


    /**
     * Async task class to get json data
     */
    private class retrieveData extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog
            pDialog = new ProgressDialog(MainActivity.this);
            pDialog.setMessage("Retrieving json data...");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... arg0) {


            // Making a request to url and getting response
            String json = jsonParser.makeHttpRequest(url);

            String image = null;
            String name = null;
            String version = null;
            String released = null;
            String api= null;
    listVersion = new ArrayList<>();
            Log.e("TAG", "Response from url: " + json.toString());

            if (json != null) {
                try {
                    JSONObject jsonObj = new JSONObject(json);

                    // Getting JSON Array node
                    JSONArray jsonArray = jsonObj.getJSONArray("versions");

                    // looping through All Version
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);

                            if(obj.has("image"))
                            {
                                image = path + obj.getString("image");
                            }else {
                                image = path;
                            }

                        name = obj.getString("name");
                        version = obj.getString("version");
                        if(obj.has("api")) {
                            api = obj.getString("api");
                        }else
                        {
                            api = "N/A";
                        }
                        if(obj.has("released")) {
                            released = obj.getString("released");
                        }else
                        {
                            released = "N/A";
                        }
                        Versions version1 = new Versions(name,version,released,api,image);
                        listVersion.add(version1);


                    }
                } catch (final JSONException e) {

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(MainActivity.this,
                                    "Json parsing error: " + e.getMessage(),
                                    Toast.LENGTH_SHORT)
                                    .show();
                        }
                    });

                }
            } else {
                Log.e("TAG", "Couldn't get json from server.");
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(MainActivity.this,
                                "Couldn't get json from server.",
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                });

            }

            return null;
        }

        @Override
        protected void onPostExecute(String file_url) {
            pDialog.dismiss();
            // Dismiss the progress dialog
            if (pDialog.isShowing())
                pDialog.dismiss();
            /**
             * Updating parsed JSON data into ListView
             * */
            DeviceAdapter.OnItemClickListener versionClickListener = new DeviceAdapter.OnItemClickListener() {
                @Override
                public boolean onItemClick(Versions versions) {
                    Intent intent = new Intent(MainActivity.this,DisplayActivity.class);
                    intent.putExtra("version",versions);

                    startActivity(intent);
                    return true;
                }

            };
            DeviceAdapter deviceAdapter = new DeviceAdapter(MainActivity.this,R.layout.list,listVersion,versionClickListener);
            listView.setAdapter(deviceAdapter);


        }

    }
}
