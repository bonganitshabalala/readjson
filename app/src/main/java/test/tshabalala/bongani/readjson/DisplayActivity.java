package test.tshabalala.bongani.readjson;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.widget.TextView;

import test.tshabalala.bongani.readjson.helper.Versions;

/**
 * Created by Bongani on 2017/02/02.
 */
public class DisplayActivity extends Activity
{
    private TextView textApi,textDate,textVersion;
    private FloatingActionButton btnBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.display_screen);

        Versions versions = (Versions) getIntent().getSerializableExtra("version");
        textApi = (TextView)findViewById(R.id.textApi);
        textDate = (TextView)findViewById(R.id.textDate);
        textVersion = (TextView)findViewById(R.id.textVersion);

        btnBack = (FloatingActionButton)findViewById(R.id.buttonBack);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               finish();
            }
        });

        textDate.setText("Released : "+versions.getReleased());
        textApi.setText("API : "+versions.getApi());
        textVersion.setText("Version : "+versions.getVersion());

    }
}
