package test.tshabalala.bongani.readjson.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import test.tshabalala.bongani.readjson.R;
import test.tshabalala.bongani.readjson.helper.Versions;

/**
 * Created by Bongani on 2017/02/02.
 */
public class DeviceAdapter extends ArrayAdapter {
    private Activity mContext = null;
    private LayoutInflater mInflater = null;
    private List<Versions> listVersion;
    private static OnItemClickListener itemClickListener;


    private static class ViewHolder {
        private ImageView mIcon = null;
        private TextView mText = null;
        private CardView cardView = null;
    }

    public DeviceAdapter(Activity context, int textViewResourceId, List<Versions> list, OnItemClickListener versionClickListener){
        super(context, textViewResourceId, list);
        this.mContext = context;
        listVersion = list;
        itemClickListener = versionClickListener;
        mInflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public View getView(int position, View contentView, final ViewGroup parent){
        final ViewHolder vh;
        final Versions ver = listVersion.get(position);
        if(contentView == null){
            vh = new ViewHolder();
            contentView = mInflater.inflate(R.layout.structure, parent, false);
            vh.mIcon = (ImageView) contentView.findViewById(R.id.imageView);
            vh.mText = (TextView) contentView.findViewById(R.id.textView);
            vh.cardView = (CardView)   contentView.findViewById(R.id.card_view);
            contentView.setTag(vh);
        } else {
            vh = (ViewHolder) contentView.getTag();
        }


        //Need to have object with text and image resource so image can also be added dynamically. ++++++++++++++++++++++++++++++++++++
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inDither = false;
        options.inJustDecodeBounds = false;
        options.inPreferredConfig = Bitmap.Config.ARGB_8888;
        options.inSampleSize = 3;
        options.inPurgeable = true;


        Picasso.with(mContext).load(ver.getImage()).into(vh.mIcon);


        vh.mText.setText(ver.getName());

        vh.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (itemClickListener != null) {
                    itemClickListener.onItemClick(ver);
                }

            }
        });

        return (contentView);
    }

    public interface OnItemClickListener{
        boolean onItemClick(Versions versions);

    }
}

